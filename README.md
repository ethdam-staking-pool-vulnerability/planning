# ethdam-staking-pool-vulnerability

## Links
- Issue board
  - https://gitlab.com/ethdam-staking-pool-vulnerability/planning/-/boards
- General
	- https://ethereum.org/en/developers/docs/mev/
	- https://docs.flashbots.net/
- Rocket Pool Exploit
	- https://www.youtube.com/watch?v=nb7x7n8Ga3U&t=2696s&ab_channel=Bankless
	- https://dao.rocketpool.net/t/a-candidate-design-for-a-modernized-penalty-system/1772
	- https://dao.rocketpool.net/t/mev-and-penalty-system/772
- MeV Rewards
	- https://boost-relay.flashbots.net/?order_by=-value
	- https://twitter.com/mevproposerbot?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1644893068007538688%7Ctwgr%5Eccaff3b5720965bf38c78e79573d2d37b253526e%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fcointelegraph.com%2Fnews%2Fethereum-validator-cashes-in-689-eth-from-mev-boost-relay
- API OAS
	- https://ethereum.github.io/builder-specs/
	- https://ethereum.github.io/beacon-APIs
	- https://flashbots.github.io/relay-specs/
- Git Repos
	- https://github.com/rocket-pool/rocketpool
	- https://github.com/ChainSafe/lodestar
- Deployed Contracts
	- ERC20.sol
		- https://etherscan.io/token/0xae78736cd615f374d3085123a210448e74fc6393#code
	- RocketSmoothingPool.sol
		- https://etherscan.io/address/0xd4E96eF8eee8678dBFf4d535E033Ed1a4F7605b7
	- RocketStorage.sol
		- https://etherscan.io/address/0x1d8f8f00cfa6758d7be78336684788fb0ee0fa46#code
	- RocketNetworkPenalties.sol
		- https://etherscan.io/address/0x9294Fc6F03c64Cc217f5BE8697EA3Ed2De77e2F8
- Statistics:
	- https://beaconcha.in/pools/rocketpool
	- https://rocketscan.io/smoothingpool
	- https://dune.com/KIV/Rocketpool
	- https://www.mevboost.org/
	- https://explore.flashbots.net/
  
## Exploit
When operating a validator node in a staking pool, there is a chance to earn extra income by capturing and retaining the Maximum Extractable Value (MeV) when its value surpasses the amount of staked ether that could potentially be deducted as a penalty. The issues of MeV theft appears to be a known but unspoken problem, recognized by protocol developers but not effectively communicated to users.

## How I stumbled upon this exploit?
As I operate a solo validator, I wanted to assess the risks associated with using Rocket Pool. In addition to the general contract risk, I discovered a very simple MeV exploit. I learned about it from a core developer's mention on the Bankless podcast. Initially, it appeared too simple to be true, but after discussing it with friends and fellow developers I met on the ethdam discord server, it seems to be a valid concern.

## Why consider joining a staking pool in the first place?
The primary challenge with operating a solo validator is the unpredictable block proposals and the resulting MeV spikes, which introduce a lottery-like element. Actual rewards can deviate significantly from the average due to the volatile nature of MeV rewards. Joining a staking pool can help smoothen the fluctuating rewards over time. Furthermore, staking pools offer the opportunity to earn fees by providing your node to others who wish to stake, without incurring any additional costs. Thus, there are compelling reasons to consider joining a staking pool. Additionally, staking pools contribute to the attractiveness of running a solo validator and thereby mitigating the risk of centralization.

## How the exploit works with RocketPool as an example?
After three MeV thefts, a 10% penalty rate is applied, increasing by 10% for each subsequent theft. Currently, the min amount to stake is 8 Ether, which is matched with 24 Ether from the pool to make a validator with 32 ether. In addition, you need to have at least 2.4 ETH worth of the RPL token as collateral. Thus, the first two times you can steal MeV without getting penalized. After that, MeV theft is only profitable when MeV > penalty. But you can simply withdraw after the second penalty and re-stake without issue to avoid the penalties.

**Sources**:
- https://dao.rocketpool.net/t/mev-and-penalty-system/772
- https://docs.rocketpool.net/guides/node/responsibilities.html#how-rocket-pool-nodes-work
- relevant contracts
  - https://github.com/rocket-pool/rocketpool/blob/master/contracts/contract/network/RocketNetworkPenalties.sol#L30
  - https://github.com/rocket-pool/rocketpool/blob/master/contracts/contract/minipool/RocketMinipoolDelegate.sol#L532
  - https://github.com/rocket-pool/rocketpool/blob/master/contracts/contract/minipool/RocketMinipoolPenalty.sol

## How big is this problem?

MeV rewards can reach significant heights, with the highest recorded MeV reward being 523 Ether. As of now, no penalties have been submitted by RocketPool's DAO, as indicated by the absence of `PenaltySubmitted` events in [RocketNetworkPenalties](https://etherscan.io/address/0x9294Fc6F03c64Cc217f5BE8697EA3Ed2De77e2F8#events).

**Questions/Todos**:
- Is there any record of MeV theft?
  - How can we determine the public keys used by RocketPool node operators for validation?
- How can we ascertain the amount of MeV that has been stolen?
  - Is there a difference between the expected APR (Annual Percentage Rate) and the actual APR?
- What is the likelihood of receiving MeV rewards exceeding 10.4 Ether (collateral at RocketPool)?

**Sources**:
- https://boost-relay.flashbots.net/?order_by=-value

## How to implement the exploit?
Currently, there is a vulnerability that allows for the theft of MeV, followed by the ability to re-stake after receiving two strikes without facing penalties. Therefore, when transitioning from a solo validator to a node operator, simply do not change your feeRecipient address at all ([RocketPool docs](https://docs.RocketPool.net/guides/node/solo-staker-migration.html#step-4-assign-the-correct-fee-recipient)). 
When utilizing RocketPool's smart node, it is necessary to make modifications to the code. Specifically, you need to ensure that the feeRecipient is set to your private address rather than the smoothingPoolContract address ([RocketPool smartnode git repo](https://github.com/rocket-pool/smartnode)).

In the future, if RocketPool starts penalizing node operators for MeV theft, it would be necessary to update the feeRecipient in the consensus client whenever the MeV surpasses the penalty amount. This update can be performed by calling the [registerValidator](https://ethereum.github.io/builder-specs/#/Builder/registerValidator), as illustrated in the following diagram:

![](exploit.sequencediagram.svg)

You can find an initial attempt to modify the Lodestar configuration [here](https://gitlab.com/ethdam-staking-pool-vulnerability/lodestar/-/commit/b9d3e3a8befba8fd040b2da26cb4791c9a21387e).

**Questions/Todos**:
- Modify lodestar client as an example for the conditional MeV theft