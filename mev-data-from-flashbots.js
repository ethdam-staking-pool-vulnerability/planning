const axios = require("axios");

const fetchData = async () => {
  const limit = 7000;
  const response = await axios.get(`https://blocks.flashbots.net/v1/blocks?limit=${limit}`);
  const blocks = response.data.blocks;

  const feeRecipientEthDiffArray = blocks.map((block) => block.fee_recipient_eth_diff / 10 ** 18);
  const diffArray = blocks.map((block) => (block.fee_recipient_eth_diff - block.eth_sent_to_fee_recipient) / 10 ** 18);

  const totalFeeRecipientEthDiff = feeRecipientEthDiffArray.reduce((acc, value) => acc + value, 0);
  const averageFeeRecipientEthDiff = Number((totalFeeRecipientEthDiff / blocks.length).toFixed(3));

  const totalEthDiff = diffArray.reduce((acc, value) => acc + value, 0);
  const averageEthDiff = Number((totalEthDiff / blocks.length).toFixed(3));

  feeRecipientEthDiffArray.sort((a, b) => a - b);
  const middleIndex = Math.floor(feeRecipientEthDiffArray.length / 2);
  const medianFeeRecipientEthDiff =
    feeRecipientEthDiffArray.length % 2 === 0
      ? Number(((feeRecipientEthDiffArray[middleIndex - 1] + feeRecipientEthDiffArray[middleIndex]) / 2).toFixed(3))
      : Number(feeRecipientEthDiffArray[middleIndex].toFixed(3));

  blocks.sort((a, b) => b.fee_recipient_eth_diff - a.fee_recipient_eth_diff);
  const highestFeeRecipientEthDiff = blocks
    .slice(0, 10)
    .map((block) => Number((block.fee_recipient_eth_diff / 10 ** 18).toFixed(3)));

  console.log(`Based on the ${limit} most recent flashbot blocks.`);
  console.log("Data retrieved at:", new Date());
  console.log("Ten highest fee_recipient_eth_diff:", highestFeeRecipientEthDiff.join(", "));
  console.log("Average fee_recipient_eth_diff:", averageFeeRecipientEthDiff);
  console.log("Average fee_recipient_eth_diff - eth_sent_to_fee_recipient:", averageEthDiff);
  console.log("Median fee_recipient_eth_diff:", medianFeeRecipientEthDiff);
};

fetchData().catch((error) => {
  console.error("An error occurred:", error);
});
